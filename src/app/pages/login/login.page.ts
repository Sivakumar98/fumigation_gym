import { ConfigService } from './../../service/config.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuController, Platform } from '@ionic/angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  pwdIcon = 'eye-off';
  showPassword: boolean = true;
  loginForm: FormGroup;
  isloader: boolean = false;
  constructor(
    private platform: Platform,
    private router: Router,
    private menuCtrl: MenuController,
    private config: ConfigService
  ) {
    this.loginForm = new FormGroup({
      mobileno: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10)
      ]),
      Password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(12)
      ])
    });
    this.platform.backButton.subscribe(async () => {
      if (this.router.isActive('/login', true) && this.router.url === '/login') {
        // navigator['app'].exitApp();
      }
    });
  }
  ngOnInit() {
    localStorage.clear();
    this.menuCtrl.enable(false);
  }
  onFormSubmit() {
    localStorage.clear();
    if (!this.loginForm.valid) {
      this.loginForm.markAllAsTouched();
      return;
    }
    console.log(this.loginForm.value);
    localStorage.setItem('GYMtoken', 'true');
    localStorage.setItem('lsUserDet', JSON.stringify(this.loginForm.value));
    this.config.toastFn(`Login Successfully ...`);
    this.router.navigateByUrl('/dashboard');
    this.loginForm.reset();

  }
  getformControl(formControl: any) {
    return this.loginForm.get(formControl) as FormArray;
  }

}
