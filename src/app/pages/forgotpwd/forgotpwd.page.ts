import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-forgotpwd',
  templateUrl: './forgotpwd.page.html',
  styleUrls: ['./forgotpwd.page.scss'],
})
export class ForgotpwdPage implements OnInit {
  forgotForm: FormGroup;
  constructor(
    private platform: Platform,
    private router: Router,
    private menuCtrl: MenuController,
  ) {
    this.forgotForm = new FormGroup({
      mobileno: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10)
      ]),
    });
   }

  ngOnInit() {
  }
  onFormSubmit() {
    localStorage.clear();
    if (!this.forgotForm.valid) {
      this.forgotForm.markAllAsTouched();
      return;
    }
  }
                                                                                                                                                                                                
  getformControl(formControl: any) {
    return this.forgotForm.get(formControl) as FormArray;
  }
}
