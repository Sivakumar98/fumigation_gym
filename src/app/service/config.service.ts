import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AlertController, LoadingController, MenuController, ToastController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';
// import { CallNumber } from '@ionic-native/call-number/ngx';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  isLoading = false;
  public rootUrl = 'http://122.165.54.203:8001/ImpalSalesAPI/api/Mobile/';
  public uploadImgUrl = 'http://122.165.54.203:8001/ImpalSalesAPI/api/upload/';
  public imgRootUrl = 'http://122.165.54.203:8001/ImpalSalesAPI/upload/';



  public noImg = '../../../assets/icon/noprofile.png';
  public ticketnoImg = '../../../assets/icon/busticket.png';
  public imgUrl = '';
  constructor(
    private http: HttpClient,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private alertController: AlertController,
    private menuCtrl: MenuController,
    private route: Router,
    private location: Location,
    private modalCtrl: ModalController,
    // private callNumber: CallNumber
  ) { }

  backFn() {
    this.location.back();
  }

  homeFn() {
    this.route.navigate(['/dashboard']);
  }

  menuFn() {
    this.menuCtrl.toggle();
  }

  // lsUserDetails() {
  //   return JSON.parse(localStorage.getItem('lsImpalUserDetail'));
  // }

  lsUserType() {
    return localStorage.getItem('lsUserType');
  }

  getData(url: any): Observable<any> {
    const apiURL = `${this.rootUrl}${url}`;
    return this.http.get(apiURL);
  }

  postData(url: any, bodyValues: any): Observable<any> {
    const apiURL = `${this.rootUrl}${url}`;
    return this.http.post(apiURL, bodyValues);
  }

  timeFn(time: any): any {
    let hour = (time.split(':'))[0];
    let min = (time.split(':'))[1];
    const part = hour > 12 ? 'PM' : 'AM';
    if (parseInt(hour) === 0)
      hour = 12;
    min = (min + '').length === 1 ? `0${min}` : min;
    hour = hour > 12 ? hour - 12 : hour;
    hour = (hour + '').length === 1 ? `0${hour}` : hour;
    return `${hour}:${min} ${part}`;
  }

  async loader(msg: any) {
    this.isLoading = true;
    return await this.loadingCtrl
      .create({
        spinner: 'crescent',
        // message : `<div class='ion-text-center'><img src='assets/loading.gif'  /> <br/> <div class='pt-2' >msg</div></div>`,
        message: msg,
        //  message : `<img src='assets/loading.gif' class='img-align' /> <br/> <div class='ion-text-center'>msg</div> `,
        // spinner: spin,  // 'bubbles' | 'circles' | 'circular' | 'crescent' | 'dots' | 'lines' | 'lines-small' | null | undefined
        // cssClass: 'my-custom-class',   // Write CSS in global.css
        // mode : 'ios',
        duration: 30000,
      })
      .then((a) => {
        a.present().then(() => {
          console.log('presented');
          if (!this.isLoading) {
            a.dismiss().then(() => console.log('abort presenting'));
          }
        });
      });
  }

  async loaderDismiss() {
    this.isLoading = false;
    return await this.loadingCtrl
      .dismiss()
      .then(() => console.log('dismissed'));
  }


  stateDetails(): Observable<any> {
    const apiURL = `${this.rootUrl}StateList`;
    return this.http.post(apiURL, {});
  }


  // async callNumberFn(no: any) {
  //     const alert = await this.alertController.create({
  //       header: 'Call',
  //       message: `Do you want to call ?`,
  //       buttons: [
  //         {
  //           text: 'No',
  //           handler: () => {
  //             console.log('Confirm Cancel: blah');
  //           },
  //         },
  //         {
  //           text: 'Yes',
  //           handler: () => {
  //             this.callNumber
  //             .callNumber(no, true)
  //             .then((res) => console.log('Launched dialer!', res))
  //             .catch((err) => console.log('Error launching dialer', err));
  //           },
  //         },
  //       ],
  //     });
  //     await alert.present();
  // }

  async toastFn(
    msg: string,
    positiontxt: any = 'bottom',
    headerString?: string
  ) {
    const toast = await this.toastCtrl.create({
      header: headerString,
      message: msg,
      position: positiontxt,
      duration: 3000,
      // cssClass: 'toastcustom',
      buttons: [
        {
          text: 'Okay',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await toast.present();
  }

  async msgAlertFn(msg: any) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      buttons: [
        {
          text: 'Okay',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel: blah');
          },
        },
      ],
    });
    await alert.present();
  }

  // async exitInternetFn(heading: any, msg: string) {
  //   const alert = await this.alertController.create({
  //     header: heading,
  //     message: msg,
  //     buttons: [
  //       {
  //         text: 'Okay',
  //         handler: () => {
  //           navigator['app'].exitApp();
  //           // console.log('Confirm Okay');
  //         },
  //       },
  //     ],
  //   });
  //   await alert.present();
  // }

  async logoutFn() {
    const alert = await this.alertController.create({
      header: 'Logout!',
      message: `Are you sure do you  want to logout !`,
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            localStorage.clear();
            this.route.navigateByUrl('/login');
          },
        },
        {
          text: 'No',
          handler: () => {
            console.log('Confirm Cancel: blah');
          },
        },
      ],
    });
    await alert.present();
  }

  async placeOrder() {
    const alert = await this.alertController.create({
      header: 'Place Order',
      message: `Do you want to place this order !`,
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.route.navigateByUrl('/dashboard');
          },
        },
        {
          text: 'No',
          handler: () => {
            console.log('Confirm Cancel: blah');
          },
        },
      ],
    });
    await alert.present();
  }

  async exitFunction(heading: any, msg: string) {
    const alert = await this.alertController.create({
      header: heading,
      message: msg,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            (navigator as any)['app'].exitApp();
            // console.log('Confirm Okay');
          },
        },
      ],
    });
    await alert.present();
  }
  async exitInternetFn(heading: any, msg: string) {
    const alert = await this.alertController.create({
      header: heading,
      message: msg,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            (navigator as any)["app"].exitApp();
            // console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }

}


